import { Module } from '@nestjs/common'
import { ElasticsearchModule } from '@nestjs/elasticsearch'

import { SearchService } from './search.service'

@Module({
  imports: [
    ElasticsearchModule.registerAsync({
      useFactory: () => ({
        node: 'http://localhost:9200'
      })
    })
  ],
  providers: [SearchService]
})
export class SearchModule {}
