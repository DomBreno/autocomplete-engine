import { Test, TestingModule } from '@nestjs/testing'
import { SearchService } from './search.service'
import { ElasticsearchModule } from '@nestjs/elasticsearch'

describe('SearchService', () => {
  let service: SearchService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ElasticsearchModule.registerAsync({
          useFactory: () => ({ node: 'http://localhost:9200' })
        })
      ],
      providers: [SearchService]
    }).compile()

    service = module.get<SearchService>(SearchService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
