FROM node:slim

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY package*.json ./
RUN npm install --silent

COPY . .

EXPOSE 3000
EXPOSE 9200

CMD npm run start:dev